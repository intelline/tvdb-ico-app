<?php
	$sitePath = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/') + 1);
	
	outputPage('page.phbr', ['docroot' => $sitePath]);

	function outputPage($templtePath, $data = []) {
		$template = file_get_contents($templtePath);

		$pageInfo = [];
		foreach ($data as $key => $value) {
			$pageInfo['keys'][] = '{{'.$key.'}}';
			$pageInfo['values'][] = $value;
		}
		echo str_replace($pageInfo['keys'], $pageInfo['values'], $template);
	}