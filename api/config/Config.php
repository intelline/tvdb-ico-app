<?php
namespace config;

class Config {

	public static $apiPath; 

	public static $db = [
		"host"     => "localhost",
		"database" => "series",
		"username" => "root",
		"password" => ""
	];

	public static $timeZone = "Europe/Sofia";

	public static $path = [
		'adminSettings' => '../custom/config.json',
		'defaultConfig' => '../custom/defaults.json',
		'cdnPath'       => '../cdn'
	];

	public static $imageExtensions = ['jpg', 'png', 'gif', 'jpeg'];
}
