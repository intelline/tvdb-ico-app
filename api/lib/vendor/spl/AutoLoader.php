<?php 
namespace spl;

require_once 'SplClassLoader.php';

$loader = new SplClassLoader();
$loader->register();