<?php
namespace lib\mine\utils;

use lib\vendor\gump\Gump;

class Helpers {

	public static function validateParams($params, $rules) {
		$isValid = GUMP::is_valid($data, $rules);
		if(!$isValid) {
			throw new Exception('Invalid params !'.$isValid[0]);
		}
	}

	public static function makeRequest($url, $method='GET', $params=[]) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);

		if ($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, $method);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		}

		$reqResponse = curl_exec($ch);
		$reqResponse = simplexml_load_string($reqResponse);
		$info = curl_getinfo($ch);

		if($info['http_code'] !== 200) {
			throw new ApiException(ApiException::HTTP_ERROR);
		}
		curl_close($ch);
		
		return $reqResponse;
	}


}