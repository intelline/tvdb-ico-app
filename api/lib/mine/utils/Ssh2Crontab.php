<?php

namespace lib\mine\utils;
 
Class Ssh2Crontab {
 
	private $connection;
	private $path;
	private $handle;
	private $cronFile;
 
	function __construct($host=null, $port=null, $username=null, $password=null) {
		$pathLength = strrpos(__FILE__, "/");      
		$this->path  = substr(__FILE__, 0, $pathLength) . '/';

		$this->handle = 'crontab.txt';
		$this->hancronFiledle =  $this->path.$this->handle;

		try{
			if(!isset($host, $port, $username, $password)) {
				throw new Exception("Please define the host, port, username and password!");
			}

			$this->connection = ssh2_connect($host, $port);
			if(!$this->connection) {
				throw new Exception("The SSH2 connection could not be established.");
			}

			$authentication = ssh2_auth_password($this->connection, $username, $password);
			 if(!$authentication) {
				throw new Exception("Could not authenticate ".$username." using password: ".$password.".");
			 }
		}
		catch(Exception $e) {
			$this->errorMessage($e->getMessage());
		}
	}
 
	public function exec() {

		try {
			if(!func_num_args()) {
				throw new Exception("There is nothing to execute."); 
			} 

			$arguments = func_get_args();
			$commands = (func_num_args() > 1) ? implode(" && ", $arguments) : $arguments[0];

			$stream = ssh2_exec($this->connection, $commands);
			if(!$stream) {
				throw new Exception("Unable to execute those commands: <br/>".$commands);
			}
		}
		catch(Exception $e) {
			$this->errorMessage($e->getMessage());
		}

		return $this;
	}
 
	public function writeToFile($path=null, $handle=null) {

		if(!file_exists($this->cron_file)) {     
			$this->handle = (!isset($handle)) ? $this->handle : $handle;
			$this->path = (!isset($path)) ? $this->path : $path;

			$this->cronFile = $this->path.$this->handle;
			$initCron = "crontab -l >". $this->cronFile." && [ -f ".$this->cronFile." ] || > ".$this->cronFile;    //Redirecting output to a file, using the > operator, will always create that file if it doesn't exist.
			$this->exec($initCron);
		}

		return $this;
	}
 
	public function removeFile() {
		if(file_exists($this->cron_file)) {
			$this->exec("rm ".$this->cronFile); 
		}

	   return $this;
	}
 
	public function appendCronjob($cronJobs=null) {
		if(!isset($cronJobs)) {
			$this->errorMessage("Please specify a cron job or an array of cron jobs.");
		} 
		$cronAppend = "echo '";
		$cronAppend .= (is_array($cronJobs)) ? implode("\n", $cronJobs) : $cronJobs;
		$cronAppend .= "' >> ".$this->cronFile;

		$cronInstall = "crontab ".$this->cron_file;
		$this->writeToFile()->exec($cronAppend, $cronInstall)->removeFile();
		 
		return $this;
	}
 
	public function removeCronjob($cronJobs=null) {
		if(!isset($cronJobs)) {
			$this->errorMessage("Please specify a cron job or an array of cron jobs.");
		}
		$this->writeToFile();
		$cronArr = file($this->cronFile, FILE_IGNORE_NEW_LINES);        //every element is a different cronJob
	  
		if(empty($cronArr)) {
			$this->errorMessage("The cronTab is already empty.");  
		} 
		$originalCount = count($cronArr);

		if(is_array($cronJobs)) {                                       //because otherwise its a single value, not an array with 1 element
			foreach($cronJobs as $cronRegex) {
				$cronArr = preg_grep($cronRegex, $cronArray, PREG_GREP_INVERT);
			}
		}
		else {
			  $cronArr = preg_grep($cronJobs, $cronArray, PREG_GREP_INVERT);
		} 

		return ($originalCount === count($cronArr)) ? $this->removeFile() : $this->removeCrontab()->appendCronjob($cronArr);
	}
 
	public function removeCrontab() {
		$this->exec("crontab -r")->removeFile();
	 
		return $this;
	}

	private function errorMessage($error) {
		echo "<pre style='color:#EE2711'>ERROR: ".$error."</pre>";
		die;
	}
}

//----------script----------(examples)------------

$crontab = new Ssh2_crontab_manager('ip', 'port', 'username', 'password');              //to be set

$crontab->appendCronjob('* 10 * * 1 path/bla/bla/command.sh >/dev/null 2>&1');          //append 1 cronjob

$cronRegex = '/path\/bla\/bla\/command.sh\/';											//regular expression for the path
$crontab->removeCronjob($cronRegex);

$cronjobs = [
	'0 0 1 * * path/bla/bla/command.sh',
	'30 8 * * 6 path/bla/bla/command.sh >/dev/null 2>&1'
];
 
$crontab->appendCronjob($cronjobs);														//append array of cronjobs


//time intervals explenation

// Minutes [0-59]
// |   Hours [0-23]
// |   |   Days [1-31]
// |   |   |   Months [1-12]
// |   |   |   |   Days of the Week [Numeric, 0-6]
// |   |   |   |   |
// *   *   *   *   * home/path/to/command/the_command.sh