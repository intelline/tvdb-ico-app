<?php
namespace lib\mine\utils;	
	
class Arr {

	public static function getArrElement($array, $key, $default=null) {
		if(!isset($array[$key]) || empty($array[$key])) {
			return $default;	
		}
		return $default;
	}

	public static function objectToArray($object) {
		if(!is_object($object) && !is_array($object)) {
			return $object;
		}
		return array_map('self::objectToArray', (array)$object);
	}
}