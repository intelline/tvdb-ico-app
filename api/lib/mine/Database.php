<?php 
namespace lib\mine;

use PDO;
use PDOException;

class Database extends PDO {

	public function __construct($host='', $database='', $user='', $pass='', $driverOptions=[]) {

		if(empty($driverOptions)) {
			$driverOptions = [
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
				PDO::ATTR_EMULATE_PREPARES => false,
				PDO::ATTR_STRINGIFY_FETCHES => false,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF 8\''
			];
		}
		try {
			parent::__construct('mysql:host='.$host.';dbname='.$database, $user, $pass);
		}
		catch(PDOException $e) {
			throw $e;
		}
	}

	//@ remove it
	// public function fetch($query, $values=[], $type ='') {
	// 	$statement = $this->prepare($query);
	// 	$statement->execute($values);
	// 	return $statement->fetch.ucfirst($type)();
	// }

	public function fetch($query, $values=[]) {
		$statement = $this->prepare($query);
		$statement->execute($values);
		return $statement->fetch();
	}

	public function fetchAll($query, $values=[]) {
		$statement = $this->prepare($query);
		$statement->execute($values);
		return $statement->fetchAll();
	}

	public function fetchColumn($query, $values=[]) {
		$statement = $this->prepare($query);
		$statement->execute($values);
		return $statement->fetchColumn();
	}

	public function fetchColumnAll($query, $values=[]) {
		$statement = $this->prepare($query);
		$statement->execute($values);
		return $statement->fetchColumnAll();
	}

	public function fetchKeyPair($query, $values=[]) {
		$statement = $this->prepare($query);
		$statement->execute($values);
		return $statement->fetchAll(PDO::FETCH_KEY_PAIR);
	}

	public function toPdoVars($fields) {
		$pdoData = [];
		foreach($fields as $pdoVar => $value) {
			$pdoData[':'.$pdoVar] = $value;
		}
		return $pdoData;
	}
}
