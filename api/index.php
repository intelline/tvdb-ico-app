<?php
session_start();

require_once 'config/Config.php';
require_once 'lib/vendor/spl/AutoLoader.php';

use config\Config;
use app\Api;

Config::$apiPath = substr($_SERVER['SCRIPT_NAME'], 1, strrpos($_SERVER['SCRIPT_NAME'], '/'));
try{
	$api = new Api();
}
catch (PDOException $e) {
	print 'Darabase Error : ' . $e->getMessage();
}
catch (Exception $e) {
	print 'Exception : ' . $e->getMessage();
}
