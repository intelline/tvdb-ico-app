<?php
namespace app;

use config\Config;
use lib\mine\Database;
use lib\mine\utils\Arr;
use lib\mine\utils\Helpers;

//Api actions

class Actions {

	public $db;

	public function __construct() {
		$this->db = new Database(Config::$db['host'], Config::$db['database'], Config::$db['username'], Config::$db['password']);
	}

	public function listSeries($seriesId=null) {
		$where = isset($seriesId) ? (' WHERE `id` = '.$seriesId) : '';

		$result = $this->db->fetch('SELECT `id`, `title`, `description`, `imdbLink`, `status`, `onAir`, `rating`, `length`, `genres`, `actors` FROM series'.$where);
		return $result;
	}

	public function listEpisodes($seriesId) {
		$seriesInfo = $this->listSeries($seriesId);
		$episodesInfo = $this->db->fetch('SELECT `id`, `title`, `season`, `seriesId` FROM episodes WHERE `seriesId` = :seriesId', [':seriesId' => $seriesId]);
		$watchedEpisodes = $this->db->fetch('SELECT `episodes` FROM user_series WHERE `seriesId` = :seriesId AND `userId` = :userId', [':seriesId' => $seriesId, ':userId' => $_SESSION['id']]);
		return [
			'series' => $seriesInfo,
			'episodes' => $episodesInfo,
			'watchedEpisodes' => $watchedEpisodes
		];
	}
}