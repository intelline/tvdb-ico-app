<?php
namespace app\exceptions;

use Exception;

class ApiException extends Exception{
	const INVALID_PARAMS    = '1';
	const INVALID_USER_PASS = '2';
	const USER_EXISTS       = '3';
	const WRONG_REQUEST     = '4';
	const NOT_LOGGED        = '5';

	private static $errMsg = [
		'1' => 'Invalid params !',
		'2' => 'Invalid username or password !',
		'3' => 'User with this username / email already exists !',
		'4' => 'Invalid url !',
		'5' => 'You are not logged !'
	];

	public function __construct($errCode, $newMsg=null) {
		$errMsg = isset($newMsg) ? $newMsg : self::$errMsg[$errCode];
		parent::__construct($errMsg, $errCode);
	}
}