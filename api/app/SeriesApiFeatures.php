<?php
namespace app;

use config\Config;
use lib\mine\Database;
use lib\mine\utils\Arr;
use lib\mine\utils\Helpers;


//Api functionality functions

class SeriesApiFeatures {

	public $db;

	public function __construct() {
		//@ database moje da se pravi o6te v API - samo 
		$this->db = new Database(Config::$db['host'], Config::$db['database'], Config::$db['username'], Config::$db['password']);
	}

	public function searchOnline($name) {
		//!@ adresa moje da e config variable
		$searchResults = Helpers::makeRequest('http://thetvdb.com/api/GetSeries.php?seriesname="'.$name.'"');
		$searchResults = Arr::objectToArray($searchResults)['Series'];

		$result = [];
		foreach($searchResults as $key => $seriesInfo) {//!@ key ne se polzva - navsqkude proveri 
			$result[$seriesInfo['seriesid']]['title'] = $seriesInfo['SeriesName'];
			$result[$seriesInfo['seriesid']]['description'] = isset($seriesInfo['Overview']) ? $seriesInfo['Overview'] : '';
		} 
		return $result;
	}

	public function addUserSeries($seriesId) {
		$exist = $this->db->fetch('SELECT `title` FROM series WHERE `id` = :id', [':id' => $seriesId]);

		if(!$exist) {
			//@ api key su6to moje da e config var
			$searchResults = Helpers::makeRequest('http://thetvdb.com/api/052D85ED5F908143/series/'.$seriesId.'/all');
			$seriesResult = Arr::objectToArray($searchResults)['Series'];

			$pdo = [
				':id'          => $seriesResult['id'],
				':title'       => $seriesResult['SeriesName'],
				':description' => !is_array($seriesResult['Overview']) ? $seriesResult['Overview'] : '',
				':imdbLink'    => !is_array($seriesResult['IMDB_ID']) ? $seriesResult['IMDB_ID'] : '',
				':status'      => !is_array($seriesResult['Status']) ? $seriesResult['Status'] : '',
				':onAir'       => !is_array($seriesResult['FirstAired']) ? $seriesResult['FirstAired'] : '',
				':rating'      => !is_array($seriesResult['Rating']) ? $seriesResult['Rating'] : '',
				':length'      => !is_array($seriesResult['Runtime']) ? $seriesResult['Runtime'] : '',
				':genres'      => !is_array($seriesResult['Genre']) ? $seriesResult['Genre'] : '',
				':actors'      => !is_array($seriesResult['Actors']) ? $seriesResult['Actors'] : '',
			];

			$columns = str_replace(':', '', implode(', ', array_keys($pdo)));
			$values = implode(', ', array_keys($pdo));

			$this->db->prepare("INSERT INTO series(".$columns.") VALUES (".$values.")")->execute($pdo);

			$episodesResult = Arr::objectToArray($searchResults)['Episode'];
			$this->addSeriesEpisodes($episodesResult);
		}

		$userInfo = [
			':userId' 	=> $_SESSION['id'],
			':seriesId' => $seriesId
		];

		$this->db->prepare("INSERT INTO user_series (`userId`, `seriesId`) 
							VALUES (:userId, :seriesId)")->execute($userInfo);
	}

	//!@ nqma6 komentari
	public function addSeriesEpisodes($episodes) {
		foreach($episodes as $key => $episode) {
			$pdo = [
				':id'          => $episode['id'],
				':title'       => $episode['EpisodeName'],
				':seriesId'    => $episode['seriesid'],
				':season'      => $episode['Combined_season']
			];

			$columns = str_replace(':', '', implode(', ', array_keys($pdo)));
			$values = implode(', ', array_keys($pdo));

			$this->db->prepare("INSERT INTO episodes (".$columns.") VALUES (".$values.")")->execute($pdo);
		}
	}

	public function removeUserSeries($seriesId) {
		$userInfo = [
			':userId'   => $_SESSION['id'],
			':seriesId' => $seriesId
		];
		$this->db->prepare('DELETE FROM user_series WHERE userId=:userId AND seriesId=:seriesId')->execute($userInfo);

		$exist = $this->db->fetch('SELECT `id` FROM user_series WHERE WHERE userId=:userId AND seriesId=:seriesId', $userInfo);

		if(!$exist) {
			unset($userInfo[':userId']);
			$this->db->prepare('DELETE FROM series WHERE id=:seriesId')->execute($userInfo);
			$this->removeSeriesEpisodes($seriesId);
		}
	}

	public function removeSeriesEpisodes($seriesId) {
		$userInfo = [
			':seriesId' => $seriesId
		];
		$this->db->prepare('DELETE FROM episodes WHERE seriesId=:seriesId')->execute($userInfo);
	}

	public function setWatchedEpisodes($seriesId, $episodes) {					
		$userInfo = [
			':user'     => $_SESSION['id'],
			':series'   => $seriesId,
			':episodes' => implode(', ', $episodes)			// ! wont override the old saves because the frontEnd will use the old values for default on series view
		];

		$this->db->prepare("UPDATE user_series SET `episodes` = :episodes WHERE `user` = :user AND `series` = :series")->execute($userInfo);
	}

	public function getWatchedEpisodes($seriesId) {
		$userInfo = [
			':userId'   => $_SESSION['userId'],
			':seriesId' => $seriesInfo['seriesId']
		];

		$episodes = $this->db->fetchColumn('SELECT `episodes` FROM user_series WHERE WHERE userId=:userId AND seriesId=:seriesId', $userInfo);
		return explode(', ', $episodes);
	}

	public function updateEpisodesDatabase() {				//to be run using cronJob script
		// * * 1 * * * /path/bla/bla
		//@ hardcodenati adresi - moje da si napravi6 const promenlivi naprimer
		$changesList = Helpers::makeRequest('http://thetvdb.com/api/Updates.php?type=all&time='.$period); 
		foreach ($changesList as $key => $data) {
			if($key == 'Episode') {							//wont be updating series because noone has the new series in favorites at that moment
				$searchResults = Helpers::makeRequest('http://thetvdb.com/api/052D85ED5F908143/episodes/'.$data['id']);

				//@ prazno ....
				$pdo = [
					//frontEnd : add needed $seriesList keys, depending on the needed info
				];
				$this->db->prepare("INSERT INTO episodes () VALUES ()")->execute($pdo);
			}
		}
	}
}
