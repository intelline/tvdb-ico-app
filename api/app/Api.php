<?php
namespace app;

use config\Config;
use lib\mine\Database;
use app\exceptions\ApiException;
use lib\vendor\router\AltoRouter;

class Api {

	public $db;

	public $router;

	public $params;

	public $features;

	public function __construct() {
		$_SESSION = [		//debug only ! to be removed after frontend login / logout()
			'id' => 1,
			'username' => 'ico'
		]; //@ sled tova ne iskam da vijdam debug ne6ta.
		$this->isLogged();

		$this->router = new AltoRouter();
		$this->router->setBasePath(Config::$apiPath);

		$this->features = new SeriesApiFeatures();

		$this->router->map('GET', '/search/[a:name]', 'search');
		$this->router->map('GET', '/add/[i:series]', 'add');
		$this->router->map('GET', '/remove/[i:series]', 'remove');
		$this->router->map('GET|POST', '/watched/[i:series]', 'watched');
		$this->router->map('GET', '/list', 'listAll');
		$this->router->map('GET', '/list/[i:series]', 'listOne');
		$this->router->map('POST', '/register', 'register');
		$this->router->map('POST', '/login', 'login');
		$this->router->map('POST', '/logout', 'logout');

		$this->match();
	}

	public function match() {
		$match = $this->router->match();

		if($match && is_callable([$this, $match['target']])) {
			$this->params = $match['params'];
			$this->data = call_user_func_array([$this, $match['target']], $match['params']);
		}
		else {
			throw new ApiException(ApiException::WRONG_REQUEST);
		}
	}

	public function isLogged() {
		$hasSession = isset($_SESSION['id'], $_SESSION['username']);
		$ignorePages = (strpos($_SERVER['REQUEST_URI'], 'login') || strpos($_SERVER['REQUEST_URI'], 'login'));

		if(!$ignorePages && !$hasSession) {	
			throw new ApiException(ApiException::NOT_LOGGED);
		}
	}

	public function login() {
		//@ Users() - priema $db - nikude ne se podava, nikude ne se suzdava.
		$user = new User(); //!@ klasa ve4e ne se kazva User a Session
		$user->login();
	}

	public function logout() {
		$user = new User();
		$user->logout();
	}

	public function register() {
		$user = new User();
		$user->register();
	}

	public function listAll() {
		$action = new Actions();
		$result = $action->listSeries();
		var_export($result);
		//frontEnd : list the result using handlebars
	}

	public function listOne() {
		$action = new Actions();
		$result = $action->listEpisodes($this->params['series']);
		var_export($result);
		
		//frontEnd : list the result using handlebars
		//parse the watched episodes response so that watched can be selected on page loading
	}

	public function search() {
		$result = $this->features->searchOnline($this->params['name']); //
		var_export($result);

		//frontEnd : list the result names and let the user choose what to add using checkboxes
		//then call api/add|delete
	}

	public function add() {
		$result = $this->features->addUserSeries($this->params['series']);
	}

	public function remove() {
		$result = $this->features->removeUserSeries($this->params['series']);
	}

	public function watched(){
		//frontEnd : let the user check the checkboxes for each episode/season and return array of values - format : [0 => '1(season) - 4(episode)']
		$result = $this->features->setWatchedEpisodes($this->params['series'], $_POST['episodes']);
	}
}
