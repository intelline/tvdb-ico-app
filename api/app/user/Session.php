<?php 
namespace app\user;

use lib\mine\Database;
use app\exceptions\ApiException;

class User { //@ da se kazva ili user ili session - faila su6to ina4e SPL ne go namira

	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function register($username, $password, $email) {
		$pdo = [
			':username' => $username,
			':password' => $email 
		];
		$userInfo = $this->db->fetch('SELECT `username`, `email` FROM users 
									  WHERE `username` = :username OR `email` = :email', $pdo);

		if(isset($userInfo)) {
			throw new ApiException(ApiException::USER_EXISTS);
		}
		else {
			$pdo['password'] = $password;
			$query = 'INSERT INTO `users`(`username`, `password`, `email`) VALUES (:username, :password, :email)';
			$this->db->prepare($query)->execute($pdo);
		}

		$this->login($username, $password);
	}

	public function login($username, $password) {
		$userInfo = $this->db->fetch('SELECT `id`, `username`, `password` FROM users
									  WHERE `username` = :user', [':user' => $username]);
		
		if(isset($userInfo) && $userInfo['password'] === md5($password)) {
			$_SESSION['id'] = $userInfo->id;
			$_SESSION['username'] = $userInfo->username;	
		}
		else {
			throw new ApiException(ApiException::INVALID_USER_PASS);
		}
	}

	public function logout() {
		if(isset($_COOKIE[session_name()])) {
			setcookie(session_name(), false, time() - 3600, '/');
		}
		session_unset();
		session_destroy();
	}
}
