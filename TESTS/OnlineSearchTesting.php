<?php

class OnlineSearch {

	public $response;

	public function search($name) {
		$response = self::makeRequest('http://thetvdb.com/api/GetSeries.php?seriesname='.$name);
		$response = self::objectToArray($response)['Series']; 
		var_export($response);
		echo '<form action="OnlineSearch.php" method="post">
		<table>';
		foreach($response as $key => $seriesInfo) {
			print '<tr><td>'. $seriesInfo['SeriesName'].'</td><td> <input type="checkbox" name="series[]" value="'.$seriesInfo['seriesid'].'"></td></tr>';
		}
		echo '<tr><td><input type="submit" value="Add to my list"></td></td><td><input type="submit" value="Generate new" <a onclick="history.back();"></a><br/></td></tr>';
		echo '</table></form>';
	}	

	static function makeRequest($url, $method='GET', $params=[]) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);

		// Set if POST request
		if ($method == 'POST') {
			curl_setopt($ch, CURLOPT_POST, $method);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		}

		// Execute curl request
		$reqResponse = curl_exec($ch);
		$reqResponse = simplexml_load_string($reqResponse);
		$info = curl_getinfo($ch);

		// on http error
		if($info['http_code'] !== 200) {
			throw new ApiException(ApiException::HTTP_ERROR);
		}

		curl_close($ch);
		
		return $reqResponse;
	}

	static function objectToArray($object) {
		if(!is_object($object) && !is_array($object)) {
			return $object;
		}
		return array_map('self::objectToArray', (array) $object);
	}
}

$search = new OnlineSearch();
if(isset($_POST['search'])) {
	$search->search($_POST['search']);
}
else {
	foreach($search->response as $key => $seriesInfo) {
		if(in_array($seriesInfo['seriesid'], $_POST['series'])){
			var_export($seriesInfo['SeriesName']);
		}
	}
}


